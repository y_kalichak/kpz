﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YuriiKalichak.Interpeter.Model;

namespace YuriiKalichak.Interpreter.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var text = Console.ReadLine();
                var lexer = new Lexer(text);
                var interpreter = new Parser(lexer);
                Console.WriteLine(interpreter.Expression());
            }
        }
    }
}
