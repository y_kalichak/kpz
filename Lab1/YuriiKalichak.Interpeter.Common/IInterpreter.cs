﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.Interpeter.Common
{
    public interface IInterpreter
    {
        object[] Execute(string programText, object[] args);
    }
}
