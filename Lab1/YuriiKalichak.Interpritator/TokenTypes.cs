﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.Interpeter.Model
{
    [Flags]
    public enum TokenTypes
    {
        EOF,
        Integer,
        Plus,
        Minus,
        Division,
        Multiplication,
        LeftParen,
        RightParen
    }
}
