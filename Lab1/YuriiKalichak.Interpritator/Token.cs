﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.Interpeter.Model
{
    public class Token
    {
        public Token(TokenTypes type, object value)
        {
            Value = value;
            Type = type;
        }

        public TokenTypes Type { get; }

        public object Value { get; }
    }
}
