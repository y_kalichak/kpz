﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.Interpeter.Model
{
    public class Parser
    {
        private Token _currentToken;
        private Lexer _lexer;

        public Parser(Lexer lexer)
        {
            _lexer = lexer;
            _currentToken = lexer.GetNextToken();
        }

        public object Expression()
        {
            var result = this.Term();
            while(TokenTypes.Plus == _currentToken.Type || TokenTypes.Minus == _currentToken.Type)
            {
                var token = _currentToken;
                if (token.Type == TokenTypes.Plus)
                {
                    this.Process(TokenTypes.Plus);
                    result = result + this.Term();
                }
                else if(token.Type == TokenTypes.Minus)
                {
                    this.Process(TokenTypes.Minus);
                    result = result - this.Term();
                }
            }
            return result;
        }


        private int Term()
        {
            var result = this.Factor();

            while (_currentToken.Type == TokenTypes.Multiplication || _currentToken.Type == TokenTypes.Division)
            {
                var token = _currentToken;
                if (token.Type == TokenTypes.Multiplication)
                {
                    this.Process(token.Type);
                    result = result * this.Factor();
                }
                else if (token.Type == TokenTypes.Division)
                {
                    this.Process(token.Type);
                    result = result / this.Factor();
                }
            }
            return result;
        }

        private int Factor()
        {
            var token = _currentToken;
            if (token.Type == TokenTypes.Integer)
            {
                this.Process(TokenTypes.Integer);
                return (int)token.Value;
            }
            else if (token.Type == TokenTypes.LeftParen)
            {
                this.Process(TokenTypes.LeftParen);
                var result = this.Expression();
                this.Process(TokenTypes.RightParen);
                return (int)result;
            }
            return 0;
        }
  

        private void Process(TokenTypes tokenType)
        {
            if (_currentToken.Type == tokenType)
            {
                _currentToken = _lexer.GetNextToken();
            }
        }
    }
}
