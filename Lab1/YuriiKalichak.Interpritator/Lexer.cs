﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.Interpeter.Model
{
    public class Lexer
    {
        private string _text;
        private int _position;
        private char? _currentChar;

        public Lexer(string text)
        {
            _text = text;
            _position = 0;
            _currentChar = new char?(_text[_position]);
        }

        private void Advance()
        {
            _position++;
            if (_position > _text.Length - 1)
            {
                _currentChar = null;
            }
            else
            {
                _currentChar = _text[_position];
            }
        }

        private void SkipWhitespace()
        {
            while (_currentChar != null && char.IsWhiteSpace(_currentChar.Value))
            {
                this.Advance();
            }
        }

        public int Integer()
        {
            int result = 0;
            while (_currentChar != null && char.IsDigit(_currentChar.Value))
            {
                result += _currentChar.Value - '0';
                this.Advance();
            }
            return result;
        }


        public Token GetNextToken()
        {
            while (_currentChar != null)
            {
                if (char.IsWhiteSpace(_currentChar.Value))
                {
                    this.SkipWhitespace();
                    continue;
                }

                if (char.IsNumber(_currentChar.Value))
                {
                    return new Token(TokenTypes.Integer, this.Integer());
                }

                if (_currentChar.Value == '+')
                {
                    this.Advance();
                    return new Token(TokenTypes.Plus, '+');
                }

                if (_currentChar.Value == '-')
                {
                    this.Advance();
                    return new Token(TokenTypes.Minus, '-');
                }
                if (_currentChar.Value == '*')
                {
                    this.Advance();
                    return new Token(TokenTypes.Multiplication, '*');
                }
                if (_currentChar.Value == '/')
                {
                    this.Advance();
                    return new Token(TokenTypes.Division, '/');
                }
                if (_currentChar.Value == '(')
                {
                    this.Advance();
                    return new Token(TokenTypes.LeftParen, '(');
                }
                if (_currentChar.Value == ')')
                {
                    this.Advance();
                    return new Token(TokenTypes.RightParen, ')');
                }
                throw new Exception("Invalid token");
            }

            return new Token(TokenTypes.EOF, null);
        }
    }
}
